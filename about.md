## Our Current Setup
Currently all our servers, including our webserver, are running on a desktop PC repurposed as a serverbox in my living room, running 24/7. We tend to turn quieter servers off when they're not being used. If you're interested in a server coming online, don't hesitate to ask!  
## Discord
Our Discord server is where you can communicate with everyone! We have a chat for every game we like to play, including games we can't run servers for such as Overwatch. When you join the Discord server, don't forget to ask for roles so you can get access to these!  
If there's a server crash or player abuse, the fastest way to contact one of us will usually be via Discord. Even if I'm out and about, I can control the whole system from my phone.
## Donations
Donations received will go towards building the server, whether it be new parts, remote hosting, or even small things such as the domain. You can give us your support on Patreon and receive some perks for helping us out!

