## Welcome to ResinDrakeNet!
We're a small community of gamers run by Resin, with a few dedicated game servers and a Discord group. We're working to enlarge the number of games we play as well as the number of people on our servers.  

All our servers are public, so feel free to check out our servers list and join in on any of our gaming sessions! You can also join our Discord group to hang out with us, receive updates on our servers and join in on voice chats.  

If you need anything, please contact me (Resin) on Discord by joining our server, or directly at *Resin#0554*.

