<link rel="shortcut icon" href="favicon.ico" />
<?php
require_once "static/Parsedown.php";
$Parsedown = new Parsedown();
$colours = [
	"bg" => "#1F1F27",
	"fg" => "#27272F",
	"accent1" => "#DFEFEF",
	"accent2" => "#FF4F5F",
	"online" => "green",
	"offline" => "red"
];
$desktop_center_pc = 25;
$server_info = [
	"fc" => [
		"display" => "Factorio",
		"domain" => "fc.resindrake.net"
	],
	"gmod" => [
		"display" => "Garry's Mod",
		"domain" => "gmod.resindrake.net"
	],
	"mc" => [
		"display" => "Minecraft",
		"domain" => "mc.resindrake.net"
	],
	"sb" => [
		"display" => "Starbound",
		"domain" => "sb.resindrake.net"
	],
	"terra" => [
		"display" => "Terraria",
		"domain" => "terra.resindrake.net"
	],
	"tf2" => [
		"display" => "Team Fortress 2",
		"domain" => "tf2.resindrake.net"
	]
];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1">
	<style>
	@import url("https://fonts.googleapis.com/css?family=Comfortaa|Noto+Sans:400,400i,700,700i");
	* {
		border-color: inherit;
		color: inherit;
		font-family: inherit;
	}
	html {
		background-color: <?=$colours["bg"]; ?>;
		color: <?=$colours["accent1"]; ?>;
		font-family: "Noto Sans", sans;
		height: 100vh;
	}
	body {
		background-color: <?=$colours["fg"]; ?>;
		display: flex;
		flex-flow: column nowrap;
		height: 100vh;
		margin: 0 <?=$desktop_center_pc ?>vw;
	}
	header {
		display: flex;
		flex: 1;
		flex-flow: column-reverse nowrap;
		padding: 0 2rem;
	}
	#header-img {
		align-items: center;
		display: flex;
		flex-flow: row nowrap;
		max-height: 3rem;
	}
	#header-img > img {
		flex-grow: 0;
		height: 100%;
	}
	#header-img > span {
		font-family: Comfortaa, sans;
		font-size: 250%;
		line-height: 0;
		margin-right: 1rem;
	}
	#nav-container {
		display: flex;
		flex-flow: row nowrap;
		justify-content: space-between;
		margin-top: 1rem;
	}
	#lnav > a {
		margin-right: 0.5rem;
	}
	#rnav > a {
		margin-left: 0.5rem;
	}
	a {
		color: <?=$colours["accent2"]; ?>;
		padding-bottom: 0.125em;
		text-decoration: none;
	}
	a:hover {
		border-bottom: 0.1em solid <?=$colours["accent2"]; ?>;
	}
	a.current {
		border-bottom: 0.1em solid <?=$colours["accent1"]; ?>;
		color: <?=$colours["accent1"]; ?>;
	}
	main {
		border-top: 0.25rem dashed <?=$colours["bg"]; ?>;
		flex: 9;
		margin-top: 1rem;
		overflow-x: hidden;
		padding: 0 2rem;
/*		width: <?=100 - 2 * $desktop_center_pc ?>vw; */
	}
	section {
		background-color: <?=$box_colour ?>;
		display: none;
		height: 100%;
		width: <?=100 - 2 * $desktop_center_pc ?>vw;
	}
	#server-list {
		align-content: center;
		display: flex;
		flex-flow: row wrap;
		justify-content: center;
	}
	.game-card {
		height: 4rem;
		margin: 1rem;
		width: 12rem;
	}
	.game-card > .background {
		background-clip: padding-box;
		background-color: black;
		background-repeat: no-repeat;
		background-size: cover;
		filter: saturate(40%) blur(0.05rem);
		height: 4rem;
		width: 12rem;
	}
	.game-card > .background-border {
		border: 0.25rem solid <?=$box_colour ?>;
		border-radius: 0.8rem;
		bottom: 4.25rem;
		height: 4rem;
		position: relative;
		right: 0.25rem;
		width: 12rem;
	}
	.game-card > .text {
		bottom: 8.25rem;
		color: white;
		height: 4rem;
		font-weight: bold;
		position: relative;
		text-align: center;
		text-shadow: 0.1rem 0.1rem 0.15rem #000000;
		width: 12rem;
	}
	.online {
		color: <?=$colours["online"] ?>;
		text-shadow: 0.05rem 0.05rem 0.1rem #000000;
	}
	.offline {
		color: <?=$colours["offline"] ?>;
		text-shadow: 0.05rem 0.05rem 0.1rem #000000;
	}
	table {
		border-collapse: collapse;
		border-color: <?=$colours["accent1"] ?>;
	}
	th {
		color: <?=$colours["accent2"] ?>;
	}
	</style>
</head>
<body>
	<header>
		<div id="nav-container">
			<div id="lnav">
				<a href="#home" id="home" onclick="switchTo('home')">Home</a>
				<a href="#servers" id="servers" onclick="switchTo('servers')">Servers</a>
				<a href="#about" id="about" onclick="switchTo('about')">About</a>
			</div>
			<div id="rnav">
				<a href="#discord" id="discord" onclick="switchTo('discord')">Discord</a>
				<a href="https://www.patreon.com/ResinDrakeNet" id="patreon">Patreon</a>
			</div>
		</div>
		<div id="header-img"><img alt="ResinDrakeNet logo" src="/static/img/Resindrake_logo.png"><span> ResinDrakeNet</span></div>
	</header>

	<main>
		<section id="home-s"><?=$Parsedown->text(file_get_contents("home.md")) ?></section>
		<section id="servers-s"><?php
		$Parsedown->text(file_get_contents("servers.md"));
		echo "<br><div id='server-list'>\n";
		foreach ($server_info as $id => $data) {
			echo "<div class='game-card'>\n";
			echo "<div class='background' style=\"background-image: url('/static/img/$id.png');\"></div>";
			echo "<div class='background-border'></div>\n";
			echo "<div class='text'><span>".$data["display"]."<br>".$data["domain"]."<br><span class='".(shell_exec("systemctl is-active gameserver@$id.service") == "active" ? "online'>online" : "offline'>offline")."</span></span></div>\n";
			echo"</div>\n";
		}
		?></div></section>
		<section id="about-s"><?=$Parsedown->text(file_get_contents("about.md")) ?></section>

		<section id="discord-s">
			<br><br><br><br>
			<center>
			<iframe src="https://discordapp.com/widget?id=606441889595654165&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>
			</center>
		</section>
		<section id="patreon-s">
			<p>Patreon</p>
		</section>
	</main>

	<script>
	"use strict";
	window.d = document; d.gEBI = d.getElementById;
	if (navigator.userAgent.includes("Mobile")) d.body.style.setProperty("margin", "0");
	let currentID;
	switch (location.hash) {
		case "#about": currentID = "about"; break;
		case "#discord": currentID = "discord"; break;
		case "#servers": currentID = "servers"; break;
		case "#home":
		default:
			currentID = "home";
	}
	const switchTo = function(sectionID) {
		d.gEBI(currentID + "-s").style.display = "none";
		d.gEBI(currentID).classList.remove("current");
		currentID = sectionID;
		d.gEBI(currentID + "-s").style.display = "unset";
		d.gEBI(currentID).classList.add("current");
	};
	switchTo(currentID);
	for (let e of Array.from(document.getElementsByTagName("table"))) e.border = 1;
	</script>
</body>
</html>
